# Lenovo_boost_WebGL    
​
### Info
Current project created in Unity 2019.4.13f1	
​
## Requirements
- Unity version 2019.4.13f1    
- Any browser whith WebGl support
​
### Plugins

| Plugin name | Version | Notes |
| ------------- | ------------- | --------- |
| DOTween | ver. 1.2.420 | [Link](https://assetstore.unity.com/packages/tools/animation/dotween-hotween-v2-27676) |
| ResourceChecker | ver. 1.0 | [Link](https://github.com/handcircus/Unity-Resource-Checker) |
| Cinemachine | ver. 2.5.0 | [Link](https://unity.com/ru/unity/features/editor/art-and-design/cinemachine) |
| Odin Inspector | ver 2.1.12.0 | [Link](https://assetstore.unity.com/packages/tools/utilities/odin-inspector-and-serializer-89041) |

### Notes
​
- To create a build u can use File/BuildSettings, open Project Settings/Player/Resolution and Presentation/WebGL Template choose the scene which you want to test and start build process.    
- To create build in easier way use navigation panel/MyTools and choose which scene u would like to make the same test build.    
- To run project for test, can be used services like GithubPages(or GitlabPages) or local server.    
- Project has a smartlook analitics connected to customer account, that's why for analitics test very useful is create your own account on this service. Analitics gather's only from mainscene, so in case of testing it, in index.html find field where smartlook is initializing and change project key to your own.    

### Useful Links

- [How to Share a Unity WebGL game on GithubPages](https://www.youtube.com/watch?v=CrsG_v1lCiQ)    
- [How to Publish a Website with GitLab Pages](https://www.youtube.com/watch?v=TWqh9MtT4Bg)    
- [Smartlook](https://www.smartlook.com/)

